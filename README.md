# Sendcloud PHP Client

<p><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></p>

Installation
------------

For now, we can register it by appending
```
"komma/sendcloud-php-client": "dev-main"
```

Of course run ``composer install`` to get these dependencies added to your vendor directory. Or ``composer update`` if you already have a composer.lock file present.


### TODO make read me