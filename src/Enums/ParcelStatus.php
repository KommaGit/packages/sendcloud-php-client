<?php

namespace Komma\Sendcloud\Enums;

enum ParcelStatus: int
{
    const NOT_SORTED = 6;

    const ERROR_COLLECTING = 15;

    const AT_CUSTOMS = 62989;

    const DELIVERY_METHOD_CHANGED = 62993;

    const AT_SORTING_CENTER = 62990;

    const ANNOUNCEMENT_FAILED = 1002;

    const REFUSED_BY_RECIPIENT = 62991;

    const RETURNED_TO_SENDER = 62992;

    const DELIVERY_ADDRESS_CHANGED = 62995;

    const CANCELLATION_REQUESTED = 1999;

    const EXCEPTION = 62996;

    const DELIVERY_DATE_CHANGED = 62994;

    const CANCELLED_UPSTREAM = 1998;

    const READY_TO_SEND = 1000;

    const ADDRESS_INVALID = 62997;

    const AWAITING_CUSTOMER_PICKUP = 12;

    const DELIVERED = 11;

    const SHIPMENT_COLLECTED_BY_CUSTOMER = 93;

    const PARCEL_EN_ROUTE = 91;

    const UNABLE_TO_DELIVER = 80;

    const SHIPMENT_PICKED_UP_BY_DRIVER = 22;

    const ANNOUNCED_NOT_COLLECTED = 13;

    const DELIVERY_ATTEMPT_FAILED = 8;

    const BEING_SORTED = 7;

    const SORTED = 5;

    const DELIVERY_DELAYED = 4;

    const EN_ROUTE_TO_SORTING_CENTER = 3;

    const ANNOUNCED = 1;

    const UNKNOWN_STATUS = 1337;

    const NO_LABEL = 999;

    const BEING_ANNOUNCED = 1001;

    const CANCELLED = 2000;

    const SUBMITTING_CANCELLATION_REQUEST = 2001;

    const DRIVER_EN_ROUTE = 92;
}
