<?php

namespace Komma\Sendcloud;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Komma\Sendcloud\Endpoints\LabelEndpoint;
use Komma\Sendcloud\Endpoints\ParcelEndpoint;
use Komma\Sendcloud\Endpoints\ServicePointEndpoint;
use Komma\Sendcloud\Endpoints\ShippingMethodEndpoint;
use Komma\Sendcloud\Endpoints\TrackingEndpoint;
use Psr\Http\Message\ResponseInterface;

class SendcloudApi
{
    /**
     * HTTP Methods
     */
    const HTTP_GET = 'GET';

    const HTTP_POST = 'POST';

    const HTTP_PUT = 'PUT';

    const HTTP_DELETE = 'DELETE';

    const HTTP_PATCH = 'PATCH';

    /**
     * Endpoints of the remote API.
     */
    const API_ENDPOINT = 'https://panel.sendcloud.sc/api';

    /**
     * Version of the remote API.
     */
    const API_VERSION = 'v2';

    /**
     * By enabling the ResourceFactory will notify missing attribute of the resources
     *
     * @var bool
     */
    public static bool $debug = false;

    private string $publicKey;

    private string $secretKey;

    private ?string $partnerId;

    protected ClientInterface $client;

    /**
     * List of Endpoints
     */
    public ParcelEndpoint $parcels;

    public TrackingEndpoint $tracking;

    public ShippingMethodEndpoint $shippingMethods;

    public ServicePointEndpoint $servicePoints;

    public LabelEndpoint $labels;

    public function __construct()
    {
        $this->loadCredentialsByLaravelSettings();

        $clientConfig = [
            'base_uri' => $this->getBaseUriEndPoint(),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'auth' => [$this->publicKey, $this->secretKey],
        ];

        if (isset($this->partnerId)) {
            $clientConfig['headers']['Sendcloud-Partner-Id'] = $this->partnerId;
        }

        $this->client = new Client($clientConfig);
        $this->initializeEndpoints();
    }

    /**
     * Try to set the API credentials by loading the laravel config
     */
    private function loadCredentialsByLaravelSettings(): void
    {
        $this->publicKey = config('services.sendcloud.public_key');
        $this->secretKey = config('services.sendcloud.secret_key');
        $this->partnerId = config('services.sendcloud.partner_id');
        self::$debug = config('services.sendcloud.debug');
    }

    /**
     * Bind the endpoints
     */
    private function initializeEndpoints()
    {
        $this->parcels = new ParcelEndpoint($this);
        $this->tracking = new TrackingEndpoint($this);
        $this->shippingMethods = new ShippingMethodEndpoint($this);
        $this->servicePoints = new ServicePointEndpoint($this);
        $this->labels = new LabelEndpoint($this);
    }

    /**
     * Get the base Uri endpoint
     *
     * @return string
     */
    private function getBaseUriEndPoint(): string
    {
        return self::API_ENDPOINT.'/'.self::API_VERSION.'/';
    }

    /**
     * Send the request to the client.
     *
     * @param Request $request
     * @param bool $parseResponse
     * @return array
     */
    public function send(Request $request, bool $parseResponse = true): array
    {
        try {
            $response = $this->client->send($request, ['http_errors' => false]);
        } catch (GuzzleException $e) {
            throw new \RuntimeException('Sendcloud error: '.$e->getMessage(), $e->getCode());
        }

        if (! $response) {
            throw new \RuntimeException('Sendcloud: Did not receive API response.');
        }

        if ($parseResponse) {
            $parsedResponse = $this->parseResponseBody($response);
        }

        if (! isset($parsedResponse)) {
            return [null, $response];
        }

        return [$parsedResponse, $response];
    }

    /**
     * Get a download from the client.
     *
     * @param string $path
     * @param array $headers
     * @return string
     */
    public function download(string $path, array $headers = ['Accept' => 'application/pdf']): string
    {
        try {
            $response = $this->client->get($path, ['headers' => $headers]);
        } catch (GuzzleException $e) {
            throw new \RuntimeException('Sendcloud error: '.$e->getMessage(), $e->getCode());
        }

        if (! $response) {
            throw new \RuntimeException('Sendcloud: Did gave a response.');
        }

        return $response->getBody()->getContents();
    }

    /**
     * Parse the PSR-7 Response body
     *
     * @param ResponseInterface $response
     * @return object
     */
    private function parseResponseBody(ResponseInterface $response): object
    {
        $body = (string) $response->getBody();
        $object = @json_decode($body);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \RuntimeException("Unable to decode response: '{$body}'.");
        }

        if ($response->getStatusCode() >= 400) {
            if (isset($object) &&
                isset($object->error) &&
                isset($object->error->message)
            ) {
                if (isset($object->error->code)) {
                    if (self::$debug) {
                        dd($object);
                    }
                    throw new \RuntimeException($object->error->message, $object->error->code);
                }
                throw new \RuntimeException($object->error->message, $response->getStatusCode());
            }

            throw new \RuntimeException('Something went wrong', $response->getStatusCode());
        }

        return $object;
    }

    public static function debug(string $message): void
    {
        if (! self::$debug) {
            return;
        }

        if (function_exists('debug')) {
            debug($message);
        } else {
            Log::debug($message);
        }
    }
}
