<?php

namespace Komma\Sendcloud\Base;

abstract class ApiResource
{
    /**
     * The original api response
     *
     * @var object
     */
    protected object $_apiResponse;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected array $_dates = [];

    /**
     * The attributes that should be mapped into other classes or types
     *
     * @var array
     */
    protected array $_typeAttributes = [];

    /**
     * The attributes can be skipped during the factory building
     *
     * @var array
     */
    protected array $_skippableAttributes = [];

    /**
     * Set the original api response
     *
     * @param object $apiResponse
     * @return $this
     */
    public function setApiResponse(object $apiResponse): self
    {
        $this->_apiResponse = $apiResponse;
        if (method_exists($this, 'customFactory')) {
            $this->customFactory();
        }

        return $this;
    }

    /**
     * Determine if the given attribute is a date or date castable.
     *
     * @param string $key
     * @return bool
     */
    public function isDateAttribute(string $key): bool
    {
        return in_array($key, $this->_dates, true);
    }

    /**
     * Is the given attribute an Typed attribute
     *
     * @param string $key
     * @return bool
     */
    public function isTypeAttribute(string $key): bool
    {
        return in_array($key, array_keys($this->_typeAttributes));
    }

    /**
     * Get the Type of the given attribute
     *
     * @param string $key
     * @return string
     */
    public function getTypeOfAttribute(string $key): string
    {
        return $this->_typeAttributes[$key];
    }

    /**
     * Is the given attribute an Skippable attribute
     *
     * @param string $key
     * @return bool
     */
    public function isSkippableAttribute(string $key): bool
    {
        return in_array($key, $this->_skippableAttributes);
    }

    /**
     * @return array
     */
    public function getFilledAttributes(): array
    {
        $data = get_object_vars($this);
        $data = array_filter($data, fn ($v, $k) => ! str_starts_with($k, '_'), 1);

        return $data;
    }
}
