<?php

namespace Komma\Sendcloud\Base;

use Komma\Sendcloud\SendcloudApi;

abstract class Endpoint
{
    protected SendcloudApi $apiClient;

    protected string $resourcePath;

    protected string $resourceClass;

    /**
     * @param SendcloudApi $apiClient
     */
    public function __construct(SendcloudApi $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param array $query
     * @return string
     */
    protected function buildQueryString(array $query): string
    {
        if (empty($query)) {
            return '';
        }

        foreach ($query as $key => $value) {
            if ($value === true) {
                $query[$key] = 'true';
            }

            if ($value === false) {
                $query[$key] = 'false';
            }
        }

        return '?'.http_build_query($query, '', '&');
    }
}
