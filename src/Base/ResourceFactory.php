<?php

namespace Komma\Sendcloud\Base;

use Carbon\Carbon;
use Komma\Sendcloud\SendcloudApi;

final class ResourceFactory
{
    /**
     * Create an array of resource objects from API results
     *
     * @param array $apiResults
     * @param string $resourceClass
     * @return array
     */
    public static function createFromApiResultArray(array $apiResults, string $resourceClass): array
    {
        $collection = [];
        foreach ($apiResults as $result) {
            $collection[] = self::createFromApiResult($result, new $resourceClass);
        }

        return $collection;
    }

    /**
     * Create resource object from API result
     *
     * @param $apiResult
     * @param ApiResource $resource
     *
     * @return ApiResource
     */
    public static function createFromApiResult($apiResult, ApiResource $resource): ApiResource
    {
        $resource->setApiResponse($apiResult);

        foreach ($apiResult as $property => $value) {
            if ($resource->isSkippableAttribute($property)) {
                continue;
            }

            if (! property_exists($resource, $property)) {
                SendcloudApi::debug(
                    self::class.': Undefined property "'.$property.'" on Resource "'.get_class($resource).'"'
                );
                continue;
            }

            // Typed attributes will be handled in the render
            if ($resource->isTypeAttribute($property) && isset($value)) {
                if (is_array($value)) {
                    $assets = [];
                    foreach ($value as $asset) {
                        $assets[] = self::createTypeAttribute($property, $resource->getTypeOfAttribute($property), $asset);
                    }
                    $value = $assets;
                } else {
                    $value = self::createTypeAttribute($property, $resource->getTypeOfAttribute($property), $value);
                }
            }

            // Check if property is a date attribute, then convert it into Carbon
            if ($resource->isDateAttribute($property) && $value !== null) {
                $value = Carbon::parse($value);
            }

            $resource->{$property} = $value;
        }

        return $resource;
    }

    /**
     * Create a typed attribute.
     *
     * @param string $property
     * @param string $attribute
     * @param $value
     * @return mixed
     */
    public static function createTypeAttribute(string $property, string $attribute, $value): mixed
    {
        if (is_subclass_of($attribute, ApiResource::class)) {
            return self::createFromApiResult($value, new $attribute);
        }

        debug($attribute);

        /**
         * Attributes that have special rules for creating
         */
        switch ($attribute) {
//            case Product::class:
//                if (in_array($property, ['alternatives'])) {
//                    return self::createFromApiResult($value, new Product());
//                }
//                $apiResponse = new ApiResponse(Product::class);
//
//                return $apiResponse->fill([$value, null]);
        }

        SendcloudApi::debug(
            self::class.': Undefined type attribute resolving "'.$attribute.'". Make sure a typed attribute extends from the ApiResource.'
        );

        return null;
    }
}
