<?php

namespace Komma\Sendcloud\Base;

use GuzzleHttp\Psr7\Response;

final class ApiResponse
{
    public readonly string $resourceClassName;

    private ?string $dataKey;

    private ?Response $response;

    public $data;

    public $meta;

    private string $apiPath;

    public function __construct(string $className, string $dataKey = null)
    {
        $this->resourceClassName = $className;
        $this->dataKey = $dataKey;
    }

    /**
     * Fill the Api Response as provided by the Api
     * and then parse the data.
     *
     * @param array $clientResponse
     * @return self
     */
    public function fill(array $clientResponse): self
    {
        [$body, $response] = $clientResponse;
        $this->response = $response;

        if (isset($this->response)) {
            switch ($this->response->getStatusCode()) {
                case 204:
                    $this->meta = null;
                    $this->data = null;

                    return $this;

                default:
                    break;
            }
        }

        if (isset($this->dataKey)) {
            if (! isset($body->{$this->dataKey})) {
                throw new \UnexpectedValueException(self::class.': The desired data key "'.$this->dataKey.'" is missing in the response or incorrect.');
            }

            $this->data = $body->{$this->dataKey};

            $bodyProps = array_keys((array) $body);

            // Move all other body properties to meta
            if (count($bodyProps) > 1) {
                $meta = [];
                foreach ($bodyProps as $bodyProp) {
                    if ($this->dataKey === $bodyProp) {
                        continue;
                    }
                    $meta[$bodyProp] = $body->{$bodyProp};
                }
                $this->meta = (object) $meta;
            } else {
                $this->meta = null;
            }
        } else {
            $this->data = $body;
        }

        $this->parse();

        return $this;
    }

    /**
     * Parse the response data into the defined Resource Class
     *
     * @return ApiResponse
     */
    private function parse(): self
    {
        if (is_array($this->data)) {
            $this->data = ResourceFactory::createFromApiResultArray($this->data, $this->resourceClassName);
        } else {
            $this->data = ResourceFactory::createFromApiResult($this->data, new $this->resourceClassName);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getApiPath(): string
    {
        return $this->apiPath;
    }

    /**
     * @param string $apiPath
     */
    public function setApiPath(string $apiPath): void
    {
        $this->apiPath = $apiPath;
    }
}
