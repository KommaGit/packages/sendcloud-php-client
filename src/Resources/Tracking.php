<?php

namespace Komma\Sendcloud\Resources;

use Carbon\Carbon;
use Komma\Sendcloud\Base\ApiResource;

class Tracking extends ApiResource
{
    protected array $_dates = [
        'created_at',
        'expected_delivery_date',
    ];

    protected array $_typeAttributes = [
        'statuses' => TrackingStatus::class,
    ];

    public string $parcel_id;

    public ?string $sendcloud_tracking_url;

    public Carbon $created_at;

    public string $carrier_code;

    public string $carrier_tracking_url;

    public bool $is_return;

    public bool $is_to_service_point;

    public bool $is_mail_box;

    public Carbon $expected_delivery_date;

    public array $statuses;

    /**
     * Get status that is marked with the delivered flag
     *
     * @return TrackingStatus|null
     */
    public function getDeliveredStatus(): ?TrackingStatus
    {
        foreach ($this->statuses as $status) {
            // TODO: Put this in an TrackingStatusEnums => because they diver from
            if ($status->parent_status === 'delivered') {
                return $status;
            }
        }

        return null;
    }

    /**
     * Check if tracking has status with delivered
     *
     * @return bool
     */
    public function isDelivered(): bool
    {
        return $this->getDeliveredStatus() !== null;
    }
}
