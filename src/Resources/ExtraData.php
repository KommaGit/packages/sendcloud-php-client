<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;

class ExtraData extends ApiResource
{
    public string $partner_name;

    public string $sales_channel;

    public string $terminal_type;

    public string $retail_network_id;

    public string $region_id;

    public string $is_active;

    public ?string $shop_type;
}
