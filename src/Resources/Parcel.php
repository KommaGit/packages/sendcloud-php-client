<?php

namespace Komma\Sendcloud\Resources;

use Carbon\Carbon;
use Komma\Sendcloud\Base\ApiResource;
use Komma\Sendcloud\SendcloudApi;

class Parcel extends ApiResource
{
    protected array $_skippableAttributes = [
        'data',
        'address_divided',
        'reference',
        'customs_declaration',
        'external_order_id',
        'external_shipment_id',
        'note',
        'to_post_number',
        'shipment_uuid',
        'shipping_method',
    ];

    protected array $_typeAttributes = [
        'country' => Country::class,
        'label' => Label::class,
        'documents' => Document::class,
        'parcel_items' => ParcelItem::class,
    ];

    protected array $_dates = [
        'date_created',
        'date_announced',
        'date_updated',
    ];

    public int $id;

    public ?int $contract;

    public string $name;

    public string $company_name;

    public string $telephone;

    public string $email;

    public string $address;

    public string $address_2;

    public string $house_number;

    public string $city;

    public string $postal_code;

    public Country|string $country;

    public ?string $to_state;

    public ?string $shipping_method_checkout_name;

    public ?string $to_service_point;

    public string $customs_invoice_nr;

    public ?int $customs_shipment_type;

    public Carbon $date_created;

    public ?Carbon $date_announced;

    public Carbon $date_updated;

    public array $parcel_items;

    public ?string $type;

    public ?object $shipment;

    public object $status;

    public object $carrier;

    public Label $label;

    public bool $is_return;

    public string $tracking_number;

    public string $tracking_url;

    public string $order_number;

    public ?string $external_reference;

    public string $colli_tracking_number;

    public string $colli_uuid;

    public int $collo_nr;

    public int $collo_count;

    public ?string $awb_tracking_number;

    public ?int $box_number;

    public int $insured_value;

    public int $total_insured_value;

    public ?int $total_order_value;

    public ?string $total_order_value_currency;

    public ?int $quantity;

    public string $weight;

    public ?string $length;

    public ?string $width;

    public ?string $height;

    public array $documents;

    public array $_from_info;

    /**
     * Get the tracking of this parcel
     *
     * @return Tracking
     */
    public function getTracking(): Tracking
    {
        if (empty($this->tracking_number)) {
            throw new \BadMethodCallException('Parcel has not tracking number');
        }

        $sendcloudApi = new SendcloudApi();
        $response = $sendcloudApi->tracking->get($this->tracking_number);

        return $response->data;
    }

    /**
     * Mark the parcel that it should use its shipping rules
     *
     * @return void
     */
    public function useShippingRules(): void
    {
        /**
         * Magic ID for Unstamped letter
         * This will bypass the required shipping_method; see docs below
         *
         * @link https://sendcloud.dev/docs/shipping/shipping-rules/
         */
        $this->shipment = (object) [
            'id' => 8,
        ];

        // Note: We only enable this for real on production
        $this->apply_shipping_rules = app()->environment('production');
    }

    /**
     * Set from information and enable the is return
     *
     * @param array $fromInfo
     * @return void
     */
    public function setFromInfo(array $fromInfo): void
    {
        $this->is_return = true;
        $this->_from_info = $fromInfo;
    }

    public function getFilledAttributes(): array
    {
        $data = parent::getFilledAttributes();

        if (isset($this->_from_info)) {
            $data = array_merge($data, $this->_from_info);
        }

        return $data;
    }
}
