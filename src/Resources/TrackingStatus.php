<?php

namespace Komma\Sendcloud\Resources;

use Carbon\Carbon;
use Komma\Sendcloud\Base\ApiResource;

class TrackingStatus extends ApiResource
{
    protected array $_dates = [
        'carrier_update_timestamp',
    ];

    public Carbon $carrier_update_timestamp;

    public string $parcel_status_history_id;

    public string $parent_status;

    public string $carrier_code;

    public string $carrier_message;
}
