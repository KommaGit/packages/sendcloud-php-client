<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;
use Komma\Sendcloud\SendcloudApi;

class Label extends ApiResource
{
    public string $label_printer;

    public array $normal_printer;

    public function downloadForNormalPrinter(int $position = 0)
    {
        if (! isset($this->normal_printer[$position])) {
            throw new \BadMethodCallException('Position '.$position.' not found as option in the normal label array.');
        }

        $sendcloudApi = new SendcloudApi();

        return $sendcloudApi->download($this->normal_printer[$position]);
    }

    public function downloadForLabelPrinter()
    {
        $sendcloudApi = new SendcloudApi();

        return $sendcloudApi->download($this->label_printer);
    }
}
