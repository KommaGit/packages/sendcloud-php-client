<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;

class ParcelItem extends ApiResource
{
    protected array $_skippableAttributes = [
        'return_reason',
    ];

    public string $description;

    public string $weight;

    public int $quantity;

    public float $value;

    public string $hs_code;

    public ?string $origin_country;

    public object $properties;

    public string $product_id;

    public string $sku;

    public ?string $item_id;

    public ?string $return_message;
}
