<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;

class ShippingMethod extends ApiResource
{
    protected array $_typeAttributes = [
        'countries' => Country::class,
    ];

    public int $id;

    public string $name;

    public string $carrier;

    public string $min_weight;

    public string $max_weight;

    public string $service_point_input;

    public float $price;

    public array $countries;
}
