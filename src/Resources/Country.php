<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;

class Country extends ApiResource
{
    public ?int $id;

    public string $name;

    public string $iso_2;

    public string $iso_3;

    public ?float $price;

    public ?float $lead_time_hours;
}
