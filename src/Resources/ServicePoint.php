<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;

class ServicePoint extends ApiResource
{
    protected array $_typeAttributes = [
        'extra_data' => ExtraData::class,
    ];

    public int $id;

    public string $code;

    public bool $is_active;

    public ?string $shop_type;

    public ExtraData $extra_data;

    public string $name;

    public string $street;

    public string $house_number;

    public string $postal_code;

    public string $city;

    public string $latitude;

    public string $longitude;

    public string $email;

    public string $phone;

    public string $homepage;

    public string $carrier;

    public string $country;

    public object $formatted_opening_times;

    public string $open_tomorrow;

    public string $open_upcoming_week;

    public string $distance;

    /**
     * Return name without code if present
     *
     * @return string
     */
    public function getName(): string
    {

        if(str_contains($this->name, $this->code)) {
            return trim(str_replace(' '.$this->code, '', $this->name));
        }

        return $this->name;
    }
}
