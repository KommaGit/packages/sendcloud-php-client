<?php

namespace Komma\Sendcloud\Resources;

use Komma\Sendcloud\Base\ApiResource;

class Document extends ApiResource
{
    public string $type;

    public string $size;

    public string $link;
}
