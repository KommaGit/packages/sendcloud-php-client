<?php

namespace Komma\Sendcloud\Endpoints;

use GuzzleHttp\Psr7\Request;
use Komma\Sendcloud\Base\ApiResponse;
use Komma\Sendcloud\Base\Endpoint;
use Komma\Sendcloud\Resources\ShippingMethod;
use Komma\Sendcloud\SendcloudApi;

final class ShippingMethodEndpoint extends Endpoint
{
    protected string $resourcePath = 'shipping_methods';

    protected string $resourceClass = ShippingMethod::class;

    public function all(array $query = []): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass, 'shipping_methods');

        if (empty($query)) {
            $apiResponse->setApiPath($this->resourcePath);
        } else {
            $apiResponse->setApiPath($this->resourcePath.$this->buildQueryString($query));
        }

        return $apiResponse->fill(
            $this->apiClient->send(
                new Request(
                    SendcloudApi::HTTP_GET,
                    $apiResponse->getApiPath()
                )
            )
        );
    }
}
