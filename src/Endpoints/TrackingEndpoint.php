<?php

namespace Komma\Sendcloud\Endpoints;

use GuzzleHttp\Psr7\Request;
use Komma\Sendcloud\Base\ApiResponse;
use Komma\Sendcloud\Base\Endpoint;
use Komma\Sendcloud\Resources\Tracking;
use Komma\Sendcloud\SendcloudApi;

final class TrackingEndpoint extends Endpoint
{
    protected string $resourcePath = 'tracking';

    protected string $resourceClass = Tracking::class;

    public function get(string $id): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass);
        $apiResponse->setApiPath($this->resourcePath.'/'.$id);

        return $apiResponse->fill(
            $this->apiClient->send(
                new Request(
                    SendcloudApi::HTTP_GET,
                    $apiResponse->getApiPath()
                )
            )
        );
    }
}
