<?php

namespace Komma\Sendcloud\Endpoints;

use Komma\Sendcloud\Base\Endpoint;

final class LabelEndpoint extends Endpoint
{
    protected string $resourcePath = 'labels';

    public function multiplePdfLabels(array $ids)
    {
        return $this->apiClient->download($this->resourcePath.'/normal_printer?ids='.implode(',', $ids));
    }
}
