<?php

namespace Komma\Sendcloud\Endpoints;

use GuzzleHttp\Psr7\Request;
use Komma\Sendcloud\Base\ApiResponse;
use Komma\Sendcloud\Base\Endpoint;
use Komma\Sendcloud\Resources\Parcel;
use Komma\Sendcloud\SendcloudApi;

final class ParcelEndpoint extends Endpoint
{
    protected string $resourcePath = 'parcels';

    protected string $resourceClass = Parcel::class;

    public function all(array $query = []): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass, 'parcels');

        if (empty($query)) {
            $apiResponse->setApiPath($this->resourcePath);
        } else {
            $apiResponse->setApiPath($this->resourcePath.$this->buildQueryString($query));
        }

        return $apiResponse->fill(
            $this->apiClient->send(
                new Request(
                    SendcloudApi::HTTP_GET,
                    $apiResponse->getApiPath()
                )
            )
        );
    }

    public function get(string $id): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass, 'parcel');
        $apiResponse->setApiPath($this->resourcePath.'/'.$id);

        return $apiResponse->fill(
            $this->apiClient->send(
                new Request(
                    SendcloudApi::HTTP_GET,
                    $apiResponse->getApiPath()
                )
            )
        );
    }

    public function create(array $data): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass, ($data['quantity'] === 1) ? 'parcel' : 'parcels');
        $apiResponse->setApiPath($this->resourcePath);

        $body = ($data['quantity'] === 1) ? ['parcel' => $data] : ['parcels' => [$data]];

        return $apiResponse->fill(
            $this->apiClient->send(
                new Request(
                    SendcloudApi::HTTP_POST,
                    $apiResponse->getApiPath(),
                    [],
                    json_encode($body)
                )
            )
        );
    }
}
