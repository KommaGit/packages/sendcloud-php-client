<?php

namespace Komma\Sendcloud\Endpoints;

use GuzzleHttp\Psr7\Request;
use Komma\Sendcloud\Base\ApiResponse;
use Komma\Sendcloud\Base\Endpoint;
use Komma\Sendcloud\Base\ResourceFactory;
use Komma\Sendcloud\Resources\ServicePoint;
use Komma\Sendcloud\SendcloudApi;

final class ServicePointEndpoint extends Endpoint
{
    protected string $resourcePath = 'https://servicepoints.sendcloud.sc/api/v2/service-points';

    protected string $resourceClass = ServicePoint::class;

    public function all(array $query = []): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass);

        if (empty($query)) {
            $apiResponse->setApiPath($this->resourcePath);
        } else {
            $apiResponse->setApiPath($this->resourcePath . $this->buildQueryString($query));
        }

        // Not because the resposne from this API diverse from the rest of the api
        // We parse the response here
        list($null, $guzzleResponse) = $this->apiClient->send(
            new Request(
                SendcloudApi::HTTP_GET,
                $apiResponse->getApiPath()
            ),
            false
        );

        $body = (string) $guzzleResponse->getBody();

        if ($guzzleResponse->getStatusCode() >= 200 && $guzzleResponse->getStatusCode() < 300) {
            $servicePoints = @json_decode($body);
            $apiResponse->data = ResourceFactory::createFromApiResultArray($servicePoints, $this->resourceClass);
        }

        return $apiResponse;
    }

    /**
     * @param string $id
     * @return ApiResponse
     */
    public function get(string $id): ApiResponse
    {
        $apiResponse = new ApiResponse($this->resourceClass);
        $apiResponse->setApiPath($this->resourcePath . '/' . $id);

        return $apiResponse->fill(
            $this->apiClient->send(
                new Request(
                    SendcloudApi::HTTP_GET,
                    $apiResponse->getApiPath()
                )
            )
        );
    }
}
